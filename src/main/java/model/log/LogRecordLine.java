package model.log;

import lombok.Builder;
import lombok.Data;

import java.time.Instant;

@Data
@Builder
public class LogRecordLine {
    private Instant time;
    private LogEvent event;

    //TODO: implement an toString in accordance with task description
    @Override
    public String toString() {
        return "LogLine{" +
                ", time=" + time +
                ", event=" + event +
                '}';
    }
}
