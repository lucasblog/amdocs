package model.log;

public enum LogEvent {

    START("*********Starting scan********"),
    SCAN_DONE("Scan done. Image loaded in memory"),
    SAVING_TIF("Saving sample TIF image in share disc ..."),
    LOADING_IMG("Loading image... "),
    IMG_SHOW("Image showed in applet");

    private String event;

    LogEvent(String logValue) {
        this.event = logValue;
    }

    public String getEvent() {
        return event;
    }
}
