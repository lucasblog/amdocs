package model.log;

import lombok.Builder;
import lombok.Data;
import model.AvgTimeSummary;

import java.util.List;

/**
 * Record is a block (sequence) of @{@link LogEvent} from a log file
 * file name: MALAGA_LUCAS_10DocumentApp_xpto.log
 *
 * TIME "*********Starting scan********";
 * TIME "Scan done. Image loaded in memory";
 * TIME "Saving sample TIF image in share disc ...";
 * TIME "Image TIF saved in shared disc";
 * TIME "Loading image… ";
 * TIME "Image showed in applet";
 */
@Data
@Builder
public class LogRecord {
    private String officeName;
    private String userName;
    private Integer monthDay;
    private Integer hour;
    private AvgTimeSummary avgTimeSummary;
    private List<LogRecordLine> lines;

    //TODO: implement an toString in accordance with task description
    @Override
    public String toString() {
        return "LogFile{" +
                "officeName='" + officeName + '\'' +
                ", userName='" + userName + '\'' +
                ", monthDay=" + monthDay +
                ", hour=" + hour +
                ", summary=" + avgTimeSummary +
                ", lines=" + lines +
                '}';
    }
}
