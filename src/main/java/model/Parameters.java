package model;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class Parameters {

    private String officeName;
    private String userName;
    private String monthDay;
    private String timeInMilliseconds;

    //TODO: implement method to convert args from the main class to parameters
    public static Parameters convert(String[] args) {
        if(args == null) {
            return Parameters.builder().build();
        }
        return null;
    }

}
