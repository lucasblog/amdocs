package model;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class AvgTimeSummary {
    private double scanDocument;
    private double saveDocument;
    private double showDocument;
    private int totalOfDocuments;
}
