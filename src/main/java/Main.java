import processor.LogProcessor;
import model.Parameters;
import model.log.LogRecordLine;

import java.util.ArrayList;
import java.util.List;

public class Main {

    private static final String DIRECTORY_PATH = "/logs";

    /**
     * 1. Optional parameters will be passed as args: day, hour, office, userName.
     *      1.2 Then readLogs(..) will read over all logs in the repository which matches with the args above
     *      1.3 readLogs(..) will return a list of all log lines as output.
     *
     * 2. Based on the args passed as parameters and the Event written in each log line, till all lines be processed.
     *
     * 3. After we process which line, we will calculate the avg time and display it based on filters.
     *      3.1 Args passed as parameters on step 1 it will be used as filters.
     */
    public static void main(String[] args) {
        //STEP 1: reading from all logs based on args
        final Parameters parameters = Parameters.convert(args);
        List<LogRecordLine> allLines = readLogs(parameters, DIRECTORY_PATH);

        //STEP 2: processing each log line based on args
        LogProcessor logProcessor = new LogProcessor(new ArrayList<>(), parameters);
        for (LogRecordLine line : allLines) {
            logProcessor.processLogLine(parameters.getOfficeName(), parameters.getUserName(), parameters.getMonthDay(), parameters.getTimeInMilliseconds(), line.getEvent().getEvent());
        }

        //STEP 3: calculating avg time based on args and displaying it
        logProcessor.processLogLine();
    }

    /**
     * NOTE: assuming there's implementation for read all files (maybe a service), line by line, based on passed parameters is done.
     *
     * @param parameters
     * @param directoryPath
     * @return  List<LogLine>
     */
    private static List<LogRecordLine> readLogs(Parameters parameters, String directoryPath) {
        //TODO: implement :)
        return new ArrayList<>();
    }

}
