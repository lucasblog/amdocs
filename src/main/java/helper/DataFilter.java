package helper;

import model.Parameters;
import model.log.LogRecord;

import java.util.List;
import java.util.stream.Collectors;

public class DataFilter {

    public static List<LogRecord> filter(Parameters parameters, List<LogRecord> records) {
        List<LogRecord> filteredRecords = records.stream()
                .filter(logRecord -> parameters.getOfficeName() == null ? true : logRecord.getOfficeName().equals(parameters.getOfficeName()))
                .filter(logRecord -> parameters.getUserName() == null ? true : logRecord.getUserName().equals(parameters.getUserName()))
                .filter(logRecord -> parameters.getMonthDay() == null ? true : logRecord.getMonthDay().equals(parameters.getMonthDay()))
                .filter(logRecord -> parameters.getTimeInMilliseconds() == null ? true : logRecord.getHour().equals(parameters.getTimeInMilliseconds()))
                .collect(Collectors.toList());
        return filteredRecords;
    }
}
