package processor;

import model.AvgTimeSummary;
import model.log.LogRecord;
import model.log.LogRecordLine;

import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.stream.Collectors;

public interface AvgTimeProcessor {

    /**
     * Assuming that logs doesn't have invalid lines and there's only five options to describe a line content.
     */
    default AvgTimeSummary calculateRecordAvgTimes(LogRecord record) {
        final List<LogRecordLine> lines = record.getLines();
        return AvgTimeSummary.builder()
                .scanDocument(lines.get(0).getTime().until(lines.get(1).getTime(), ChronoUnit.MILLIS))
                .saveDocument(lines.get(2).getTime().until(lines.get(3).getTime(), ChronoUnit.MILLIS))
                .showDocument(lines.get(3).getTime().until(lines.get(4).getTime(), ChronoUnit.MILLIS)).build();
    }

    default AvgTimeSummary calculateTotalRecordsAvgTimes(List<LogRecord> filteredList) {
        //Avg time per log record
        List<AvgTimeSummary> individualAvgTimeSummary = filteredList.stream().map(record -> calculateRecordAvgTimes(record)).collect(Collectors.toList());

        //Total avg
        double scanDoc = individualAvgTimeSummary.stream().map(AvgTimeSummary::getScanDocument).mapToDouble(val -> val).average().orElse(0.0);
        double saveDisc = individualAvgTimeSummary.stream().map(AvgTimeSummary::getSaveDocument).mapToDouble(val -> val).average().orElse(0.0);
        double showDoc = individualAvgTimeSummary.stream().map(AvgTimeSummary::getShowDocument).mapToDouble(val -> val).average().orElse(0.0);
        return AvgTimeSummary.builder().scanDocument(scanDoc).saveDocument(saveDisc).showDocument(showDoc).build();
    }
}
