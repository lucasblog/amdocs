package processor;

import helper.DataFilter;
import model.Parameters;
import model.AvgTimeSummary;
import model.log.LogEvent;
import model.log.LogRecord;
import model.log.LogRecordLine;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

public class LogProcessor implements AvgTimeProcessor {

    private List<LogRecord> logRecords;
    private Parameters parameters;

    public LogProcessor() {
        logRecords = new ArrayList<>();
        parameters = Parameters.builder().build();
    }

    public LogProcessor(List<LogRecord> logRecords, Parameters parameters) {
        this.logRecords = logRecords;
        this.parameters = parameters;
    }

    /**
     * This method should be called when we don't have any parameter, as result it will calculate avg time and print it.
     */
    public void processLogLine() {
        List<LogRecord> filteredRecords = DataFilter.filter(parameters, logRecords);
        AvgTimeSummary avgTimeSummary = calculateTotalRecordsAvgTimes(filteredRecords);

        //printing general avg time
        avgTimeSummary.toString();

        //printing details
        filteredRecords.toString();
    }

    /**
     * Process log line one by one, it will be called multiple times.
     * Once the method does not receive any parameter, it means that all logs files were read and the output should be shown in screen.
     *
     * @param officeName
     * @param userName
     * @param monthDay
     * @param timeInMilliseconds
     * @param logString
     */
    public void processLogLine(final String officeName, final String userName, final String monthDay, final String timeInMilliseconds, final String logString) {
        //Build a line based on input parameters
        LogRecordLine line = LogRecordLine.builder()
                .event(LogEvent.valueOf(logString))
                .time(Instant.ofEpochSecond(convertToMillisec(timeInMilliseconds))).build();


        //If line created contains @LogEvent.START it's a new operation in the scanner and a new sequence of events will be added
        if (isStartingNewRecord(line)) {
            logRecords.add(createNewRecord(officeName, userName, monthDay, timeInMilliseconds, line));
        } else {
            logRecords.get(logRecords.size() - 1).getLines().add(line);
        }
    }

    private LogRecord createNewRecord(final String officeName, final String userName, final String monthDay, final String timeInMilliseconds, final LogRecordLine line) {
        List<LogRecordLine> lines = new ArrayList<>();
        lines.add(line);

        LogRecord newRecord = LogRecord.builder()
                .officeName(officeName)
                .userName(userName)
                .monthDay(Integer.valueOf(monthDay))
                .hour(Integer.valueOf(timeInMilliseconds.substring(0, 2)))
                .lines(lines).build();

        return newRecord;
    }

    private boolean isStartingNewRecord(final LogRecordLine line) {
        return line.getEvent() == LogEvent.START;
    }

    private long convertToMillisec(final String timeInMilliseconds) {
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
            return sdf.parse(timeInMilliseconds).getTime();
        } catch (ParseException e) {
            e.printStackTrace();
            return 0;
        }
    }
}
